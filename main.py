import argparse


def calc_odds(deck):
    streets = deck.streets
    outs = deck.outs
    cards = deck.cards
    odds = 1
    if deck.verbose:
        verbose = True
    else:
        verbose = False

    # print(f'You have {outs} outs with {cards} cards remaining and {streets} streets')
    for i in range(streets):
        odds = odds * ( (cards - outs) / cards )
        cards -= 1

        if verbose:
            print(f'You have {outs} outs with {cards} cards remaining and {streets -i} streets')
            print(f'{odds * 100:.2f}%')

    odds = (1 - odds) * 100
    formatted_odds = f'{odds:.2f}'

    print(f'Percent chance: {formatted_odds}%')


def init_parser():
    parser = argparse.ArgumentParser(description='Odds Calc', add_help=True)
    parser.add_argument('-c', '--cards',
                        help="cards remaining", type=int, required=True)
    parser.add_argument('-o', '--outs',
                        help="outs remaining", type=int, required=True)
    parser.add_argument('-s', '--streets', help="streets remaining", type=int, required=True)
    parser.add_argument('-v', '--verbose', action="store_true",
                        help="verbose logging")

    args = parser.parse_args()
    return args


if __name__ == "__main__":
    _deck = init_parser()
    calc_odds(_deck)
